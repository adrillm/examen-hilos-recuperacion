public class Ejercicio3 {
    public static Thread[] threads;
    private static String nombre = "X-Wing ";

    public static void main(String[] args) {


        int totalNaves = 10;
        int distanciaRecorreida = 20000;

        Nave[] naves = new Nave[totalNaves];
        for (int i = 0; i < naves.length; i++) {
            naves[i] = new Nave(distanciaRecorreida);

        }
        Cañon_Plasma cañon_plasma = new Cañon_Plasma();
        Thread threadCañon = new Thread(cañon_plasma);
        threads = new Thread[naves.length];
        System.out.println("Inicio");

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(naves[i], nombre + (i + 1));
            threads[i].start();
        }
        threadCañon.start();

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        System.out.println("Fin");
        System.out.println("Los X-Wing han eliminado un total de " + Escuadron.totalEquipo + " cazas");
        System.out.println("La rebelion ha completado la mision");

    }


}
