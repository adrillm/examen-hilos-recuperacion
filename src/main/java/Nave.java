import java.util.concurrent.ThreadLocalRandom;

public class Nave implements Runnable {
    int distacia_Recorrida;

    public Nave(int distacia_Recorrida) {
        this.distacia_Recorrida = distacia_Recorrida;
    }


    @Override
    public void run() {
        int muertes = 0;
        try {
            for (int i = 500; i <= distacia_Recorrida; i += 500) {
                boolean matar = ThreadLocalRandom.current().nextInt(0, 100) < 80;
                Thread.sleep(1000);
                if (!matar) {
                    System.out.println("La nava " + Thread.currentThread().getName() + " ha recorrido " + i + " y no ha alcanzado a ningun caza");
                } else {
                    Escuadron.incrementrarTotal();
                    muertes++;
                    System.out.println("La nave " + Thread.currentThread().getName() + " ha recorrido " + i + " y ha alacanzado a un caza");

                }
            }
            System.out.println(Thread.currentThread().getName() + " ha llegado a la estrella de la muerte y  ha matado a un total de " + muertes + " cazas");

        } catch (InterruptedException e) {
            System.out.println("Oh no el " + Thread.currentThread().getName() + " ha sido derribadoº");
            ;
        }

    }
}
