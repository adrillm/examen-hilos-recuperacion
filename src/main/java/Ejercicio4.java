public class Ejercicio4 {
    public static Thread[] threads;
    public static Thread[] threadsCaza;

    private static String nombre = "X-Wing ";

    public static void main(String[] args) {


        int totalNaves = 10;
        int distanciaRecorreida = 20000;
        int totalCaza = 5;

        Nave[] naves = new Nave[totalNaves];
        for (int i = 0; i < naves.length; i++) {
            naves[i] = new Nave(distanciaRecorreida);
        }

        Caza_imperial[] caza_imperials = new Caza_imperial[totalCaza];
        for (int i = 0; i < caza_imperials.length; i++) {
            caza_imperials[i] = new Caza_imperial();
        }

        Cañon_PlasmaAux cañon_plasma = new Cañon_PlasmaAux();
        Thread threadCañon = new Thread(cañon_plasma);

        threads = new Thread[naves.length];
        threadsCaza = new Thread[caza_imperials.length];
        System.out.println("Inicio");

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(naves[i], nombre + (i + 1));
            threads[i].start();
        }
        for (int i = 0; i < threadsCaza.length; i++) {
            threadsCaza[i] = new Thread(caza_imperials[i]);
            threadsCaza[i].start();
        }
        threadCañon.start();

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        for (int i = 0; i < threadsCaza.length; i++) {
            try {
                threadsCaza[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        try {
            threadCañon.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("Fin");
        System.out.println("Los X-Wing han eliminado un total de " + Escuadron.totalEquipo + " cazas");
        System.out.println("La rebelion ha completado la mision");


    }


}
