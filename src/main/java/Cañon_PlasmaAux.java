import java.util.concurrent.ThreadLocalRandom;

public class Cañon_PlasmaAux implements Runnable {


    @Override
    public void run() {
        for (int i = 0; i < 30; i++) {
            int posibles_asesinados = ThreadLocalRandom.current().nextInt(Ejercicio4.threads.length);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (probAsesinatos()) {
                if (Ejercicio4.threads[posibles_asesinados].isAlive()) {
                    System.out.println("El cañon de plama ha alcanzado a un X-Wing");
                    Ejercicio4.threads[posibles_asesinados].interrupt();

                }
            }


        }
        if (Ejercicio4.threads.length == 0) {
            System.out.println("La rebelion ha perdido");
            System.exit(0);
        }
    }

    public boolean probAsesinatos() {
        return ThreadLocalRandom.current().nextInt(0, 100) < 70;
    }
}


