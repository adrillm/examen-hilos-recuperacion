# La Batalla de Endor

Durante el transcurso de la rebelión se han ganado diversas batallas contra el Imperio, pero ahora se está recuperando y
está construyendo una nueva *Estrella de la Muerte* en la 2ª Luna de Endor ( Endor, conocido también por el planeta de
los Ewoks)

Tú misión como para con tus camaradas de la resistencia es realizar un simulador de posibles escenarios en los que
se puedan plantear posibles escenarios de batalla.

Para la misión, disponemos de 10 cazas X-Wing (Ala X) y de diversos pilotos experimentados.

## Ejercicio 1 (1p)

Todas las naves están a cubierto en la primera Luna de Endor, esperando a que el Han Solo desactive el campo de fuerza.
Una vez se desactive saldrán todas las naves al encuentro.

Dada que la distancia desde la 1ª Luna hasta la 2ª es de 20.000 Km y la velocidad de las naves sin hipervelocidad es de
500 Km/s en el espacio y que la probabilidad de que en cada segundo acaben con un *Caza Imperial* es de un 80%,
Imprime los siguientes mensajes:

* Durante la ejecución, en cada intervalo, cada X-Wing ha de imprimir su nombre, la distancia total recorrida y si ha acabado con un caza o no.
* Cuando un X-Wing haya recorrido la distancia total, ha de imprimir que ha llegado a la estrella de la muerte y cuantos cazas ha destruido.

## Ejercicio 2 (2p)

Crea una nueva versión del Ejercicio Anterior en el que se utilice una nueva clase llamada Escuadrón que se encargue 
de almacenar el número de cazas abatidos por la alianza rebelde. 

Cuando haya teminado la Ejecución de todos los cazas, se ha de imprimir este resultado.

## Ejercicio 3 (3p)

Los cañones de plasma de la Estrella de la Muerte pueden acabar con un X-Wing de un solo disparo. Estos cañones pueden 
disparar cada 2 segundos y tienen una probabilidad de un 70% de acertar a un X-Wing que se encuentre dentro de un radio de
40.000 por tanto, desde que los X-Wing despeguen.

Incorpora a la simulación en la que partas de las 10 naves iniciales, pero que además, incorpores los disparos de la 
Estrella de Muerte. Cuando un Ala-X muera por un disparo deberá mostrarlo y al final de la simulación, con que
un sólo X-Wing llegue a la Estrella de la Muerte, la misión habrá sido exitosa. 
En caso contrario, la rebelión habrá fracasado.

## Ejercicio 4 (4p)

Incopora a la simulación anterior un escuadrón de 5 *Cazas Imperiales* sean capaces de disparar, sin importar
la distancia a los X-Wing una vez cada segundo con una probabilidad de acierto del 10%. 

Realiza la simulación anterior, indicando también los mensajes de los cazas.

